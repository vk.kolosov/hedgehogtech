package kolosov.hedgehogtech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HedgehogTechApplication {

    public static void main(String[] args) {
        SpringApplication.run(HedgehogTechApplication.class, args);
    }

}
