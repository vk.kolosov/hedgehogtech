package kolosov.hedgehogtech.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //инкрементация
    private long id;

    private String name;

    private String surname;

    public UserEntity(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}
