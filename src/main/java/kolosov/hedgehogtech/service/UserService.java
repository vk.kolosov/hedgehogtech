package kolosov.hedgehogtech.service;

import kolosov.hedgehogtech.entity.UserEntity;
import kolosov.hedgehogtech.exception.UserAlreadyExistException;
import kolosov.hedgehogtech.exception.UserNotExistException;
import kolosov.hedgehogtech.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public boolean checkUser(UserEntity user) {
        if (userRepository.findUserEntityById(user.getId()) != null)
            return true;
        if (userRepository.findUserEntityByNameAndSurname(user.getName(), user.getSurname()) != null)
            return true;
        return false;
    }

    public void checkUserByNameAndSurname(String name, String surname) throws UserAlreadyExistException {
        if (userRepository.findUserEntityByNameAndSurname(name, surname) != null)
            throw new UserAlreadyExistException("Такой пользователь существует");
    }

    public void saveUser(UserEntity user) throws UserAlreadyExistException {
        if (checkUser(user) == true)
            throw new UserAlreadyExistException("Такой пользователь уже существует");
        userRepository.save(user);
    }

    public void deleteUserID(Long id) throws UserNotExistException {
        if (userRepository.findUserEntityById(id) == null)
            throw new UserNotExistException("Пользователя с таким id не существует");
            userRepository.deleteById(id);
    }

    public void deleteUser(String name, String surname) throws UserNotExistException {
        if (userRepository.findUserEntityByNameAndSurname(name, surname) == null)
            throw new UserNotExistException("Такого пользователя не существует");
            userRepository.deleteAllByNameAndSurname(name, surname);
    }

    public List<UserEntity> findUsers(String search) throws UserNotExistException {
        List<UserEntity> users = userRepository.findUserEntitiesByNameContaining(search);
        if (users.size() == 0)
            throw new UserNotExistException("Таких пользователей нет");
        return users;
    }

    public List<UserEntity> findAll() {
        List<UserEntity> users = userRepository.findAll();
        return users;
    }

}
