package kolosov.hedgehogtech.controller;

import kolosov.hedgehogtech.entity.UserEntity;
import kolosov.hedgehogtech.exception.UserAlreadyExistException;
import kolosov.hedgehogtech.exception.UserNotExistException;
import kolosov.hedgehogtech.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/users") //с этого поля будут начинаться все запросы

public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findall")
    public ResponseEntity findAll() {
        try {
            return ResponseEntity.ok(userService.findAll());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    //Добавление пользователя
    @PostMapping("/adduser")//работает
    public ResponseEntity addUser(@RequestBody UserEntity user) {
        try {
            userService.saveUser(user);
            return ResponseEntity.ok("Пользователь добавлен");
        } catch (UserAlreadyExistException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    //Проверка наличия пользователя
    @PostMapping("/checkuser") //внимание в заглавным и строчным буквам, в поиске это разные значения
    public ResponseEntity addUser(@RequestParam("name") String name,
                                  @RequestParam("surname") String surname) {
        try {
            userService.checkUserByNameAndSurname(name, surname);
            return ResponseEntity.ok("Такого пользователя нет в базе");
        } catch (UserAlreadyExistException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    //Удаление пользователя
    @PostMapping("/removeuserbyid") //работает
    public ResponseEntity removeUser(@RequestParam("id") Long id) {
        try {
            userService.deleteUserID(id);
            return ResponseEntity.ok("Пользователь удален");
        } catch (UserNotExistException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    //Удаление пользователя
    @Transactional
    @PostMapping("/removeuser") //работает
    public ResponseEntity removeUser(@RequestParam("name") String name,
                                     @RequestParam("surname") String surname) {
        try {
            userService.deleteUser(name, surname);
            return ResponseEntity.ok("Пользователь удален");
        } catch (UserNotExistException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    //Поиск по пользователям
    @GetMapping("/findusers")
    public ResponseEntity removeUser(@RequestParam("search") String search) {
        try {
            List<UserEntity> users = userService.findUsers(search);
            return ResponseEntity.ok(users);
        } catch (UserNotExistException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }


}
