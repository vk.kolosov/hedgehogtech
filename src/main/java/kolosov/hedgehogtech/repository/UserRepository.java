package kolosov.hedgehogtech.repository;

import kolosov.hedgehogtech.entity.UserEntity;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findUserEntityById(Long id);
    UserEntity findUserEntityByNameAndSurname(String name, String surname);
    void deleteAllByNameAndSurname(String name, String surname);
    List<UserEntity> findUserEntitiesByNameContaining(String search);
}
